﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class ServerActions {
    public static void getCards(System.Action<JSONObject> callback)
    {
        JSONObject request = new JSONObject(JSONObject.Type.OBJECT);
        request.AddField("action", "getCards");
        string customUrl = "play.php";
        ServerInterface.serverInterface.doPost(request, customUrl, callback);
    }

    public static void startGame(List<string> cards, System.Action<JSONObject> callback)
    {
        JSONObject request = new JSONObject(JSONObject.Type.OBJECT);
        JSONObject cardArr = new JSONObject(JSONObject.Type.ARRAY);
        foreach (string str in cards)
        {
            cardArr.Add(str);
        }
        request.AddField("action", "startGame");
        request.AddField("cards", cardArr);
        ServerInterface.serverInterface.doPost(request, callback);
    }

    public static void checkGameFull(System.Action<JSONObject> callback)
    {
        JSONObject request = new JSONObject(JSONObject.Type.OBJECT);
        request.AddField("action", "checkGameFull");
        ServerInterface.serverInterface.doPost(request, callback);
    }

    public static void checkForChanges(System.Action<JSONObject> callback)
    {
        JSONObject request = new JSONObject(JSONObject.Type.OBJECT);
        request.AddField("action", "checkForChanges");
        ServerInterface.serverInterface.doPost(request, callback);
    }

    public static void switchPhase(System.Action<JSONObject> callback)
    {
        JSONObject request = new JSONObject(JSONObject.Type.OBJECT);
        request.AddField("action", "switchPhase");
        ServerInterface.serverInterface.doPost(request, callback);
    }

    public static void endTurn(System.Action<JSONObject> callback)
    {
        JSONObject request = new JSONObject(JSONObject.Type.OBJECT);
        request.AddField("action", "endTurn");
        ServerInterface.serverInterface.doPost(request, callback);
    }

    public static void setAttackers(List<string> attackers, System.Action<JSONObject> callback)
    {
        JSONObject request = new JSONObject(JSONObject.Type.OBJECT);
        JSONObject cardArr = new JSONObject(JSONObject.Type.ARRAY);
        foreach (string id in attackers)
        {
            cardArr.Add(id);
        }
        request.AddField("action", "startGame");
        request.AddField("cards", cardArr);
        ServerInterface.serverInterface.doPost(request, callback);
    }

    public static void combat(Dictionary<string, List<string>> combats, System.Action<JSONObject> callback)
    {
        JSONObject request = new JSONObject(JSONObject.Type.OBJECT);
        JSONObject attackers = new JSONObject(JSONObject.Type.OBJECT);
        foreach (KeyValuePair<string, List<string>> combat in combats)
        {
            JSONObject defenders = new JSONObject(JSONObject.Type.ARRAY);
            foreach (string defender in combat.Value)
            {
                defenders.Add(defender);
            }
            attackers.AddField(combat.Key, defenders);
        }
        request.AddField("action", "combat");
        request.AddField("combats", attackers);
        ServerInterface.serverInterface.doPost(request, callback);
    }

    public static void activateAbility(string cardID, int abilityIndex, System.Action<JSONObject> callback)
    {
        JSONObject request = new JSONObject(JSONObject.Type.OBJECT);
        request.AddField("action", "endTurn");
        request.AddField("cardID", cardID);
        request.AddField("index", abilityIndex);
        ServerInterface.serverInterface.doPost(request, callback);
    }

    public static void activateBuilding(System.Action<JSONObject> callback)
    {
        JSONObject request = new JSONObject(JSONObject.Type.OBJECT);
        request.AddField("action", "activateBuilding");
        ServerInterface.serverInterface.doPost(request, callback);
    }

    public static void playCard(string cardID, string zone, int index, System.Action<JSONObject> callback)
    {
        JSONObject request = new JSONObject(JSONObject.Type.OBJECT);
        request.AddField("action", "playCard");
        request.AddField("cardID", cardID);
        request.AddField("zone", zone);
        request.AddField("index", index);
        ServerInterface.serverInterface.doPost(request, callback);
    }

    public static void selectTargets(List<string> targets, System.Action<JSONObject> callback)
    {
        JSONObject request = new JSONObject(JSONObject.Type.OBJECT);
        JSONObject cardArr = new JSONObject(JSONObject.Type.ARRAY);
        foreach (string id in targets)
        {
            cardArr.Add(id);
        }
        request.AddField("action", "selectTargets");
        request.AddField("cards", cardArr);
        ServerInterface.serverInterface.doPost(request, callback);
    }

    public static void changeLane(int cardID, string zone, System.Action<JSONObject> callback)
    {
        JSONObject request = new JSONObject(JSONObject.Type.OBJECT);
        request.AddField("action", "playCard");
        request.AddField("cardID", cardID);
        request.AddField("zone", zone);
        ServerInterface.serverInterface.doPost(request, callback);
    }

    public static void payDebt(System.Action<JSONObject> callback)
    {
        JSONObject request = new JSONObject(JSONObject.Type.OBJECT);
        request.AddField("action", "payDebt");
        ServerInterface.serverInterface.doPost(request, callback);
    }
}
