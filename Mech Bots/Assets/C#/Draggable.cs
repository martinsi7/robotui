﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;

public class Draggable : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler {

    public bool isDragEnabled = false;

	public Transform parentToReturnTo = null;
	public void OnBeginDrag(PointerEventData eventData) {
        if (!isDragEnabled)
        {
            return;
        }
		Debug.Log ("OnBeginDrag");
        handleDragStart();
		parentToReturnTo = this.transform.parent;
		transform.SetParent( transform.parent.parent );

		GetComponent<CanvasGroup>().blocksRaycasts = false;
	}

    protected virtual void handleDragStart()
    {

    }

    protected virtual void handleDragEnd()
    {

    }



    public void OnDrag(PointerEventData eventData) {
        if (!isDragEnabled)
        {
            return;
        }
        //Debug.Log ("OnDrag");
        transform.position = eventData.position;
	}
	
	public void OnEndDrag(PointerEventData eventData) {
        if (!isDragEnabled)
        {
            return;
        }
        Debug.Log ("OnEndDrag");
		this.transform.SetParent( parentToReturnTo );

		GetComponent<CanvasGroup>().blocksRaycasts = true;
        handleDragEnd();
	}
	


}
