﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.IO;

public class ServerInterface : MonoBehaviour {

    //private string url = "http://68.49.205.20/";
    private string url = "http://10.0.0.213/tenacious_brigade/";
    private string extension = "server_interface.php";

    public static ServerInterface serverInterface;

    public static bool requestFinished = false;
    public static bool requestSent = false;

    void Start()
    {
        serverInterface = this;
        /*string jsonStr = "{ \"array\": [1.44,2,3], " +
                        "\"object\": {\"key1\":\"value1\", \"key2\":256}, " +
                        "\"string\": \"The quick brown fox \\\"jumps\\\" over the lazy dog \", " +
                        "\"int\": 65536, " +
                        "\"float\": 3.1415926, " +
                        "\"bool\": true, " +
                        "\"null\": null }";*/
        //JSONObject json = new JSONObject(jsonStr);
        //int temp = (int) json["object"]["key2"].i;
        //print(temp);
        //print(json.Print(true));
        //print(json.Print());
    }

    public void doPost(JSONObject request, System.Action<JSONObject> callback)
    {
        request.AddField("player", GameDriver.playerName);
        StartCoroutine(makeRequest(request.Print(), callback));
    }

    public void doPost(JSONObject request, string customUrl, System.Action<JSONObject> callback)
    {
        string tempStr = extension;
        extension = customUrl;
        doPost(request, callback);
        extension = tempStr;
    }

    public IEnumerator makeRequest(string request, System.Action<JSONObject> callback)
    {
        JSONObject response = null;
        var encoding = new System.Text.UTF8Encoding();
        Dictionary<string, string> postHeader = new Dictionary<string, string>();
        postHeader.Add("Content-Type", "text/json");
        WWW requestObj = new WWW(url+"/"+extension, encoding.GetBytes(request), postHeader);
        yield return requestObj;

        print(requestObj.text);
        print(requestObj.error);
        response = new JSONObject(requestObj.text);

        if (this.hasValidReponse(response))
        {
            callback(response);
        }
    }

    public bool hasValidReponse(JSONObject response)
    {
        bool retVal = false;
        string result = response["result"].str;
        if (result == "success")
        {
            retVal = true;
        }
        else
        {
            Debug.Log("Server sent error");
            if (response["message"].str != null)
            {
                Debug.Log(response["message"].str);
            }
            retVal = false;
        }
        return retVal;
    }
}
