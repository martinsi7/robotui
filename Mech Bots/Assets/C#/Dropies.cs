﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;
public class Dropies : MonoBehaviour, IDropHandler, IPointerEnterHandler, IPointerExitHandler {

	public void	OnPointerEnter(PointerEventData evenData) {

	}

	public void OnPointerExit(PointerEventData eventData) {

	}

	public void OnDrop(PointerEventData eventData) {
        Draggable d = eventData.pointerDrag.GetComponent<Draggable>();
        print(d);
        if (d == null)
        {
            return;
        }
        if (doDropAction(d))
        {
            d.parentToReturnTo = this.transform;
        }
	}

    public virtual bool doDropAction(Draggable d) { return d.isDragEnabled; }
}
