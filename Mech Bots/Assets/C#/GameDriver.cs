﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameDriver : MonoBehaviour {
    public static GameDriver gameDriver;
    public static string playerName = "both";
    public float scaleFactor = 1.0f;
    public RectTransform canvasTransform;
    public CanvasScaler canvasScaler;

    public Dictionary<string, Card> cards;

    public List<Card> temp2;
    public float startTime;

    public GameObject fighterPrefab;
    public GameObject specialistPrefab;
    public GameObject buildingCard;
    public GameObject mechPrefab;

    public GameObject playerOneHand;
    public GameObject playerOneLeft;
    public GameObject playerOneCenter;
    public GameObject playerOneRight;
    public GameObject playerOneClinic;
    public GameObject playerOneMech;
    public GameObject playerTwoHand;
    public GameObject playerTwoLeft;
    public GameObject playerTwoCenter;
    public GameObject playerTwoRight;
    public GameObject playerTwoClinic;
    public GameObject playerTwoMech;

    public Player playerOne;
    public Player playerTwo;
    public Player activePlayer;
    public Player otherPlayer;
    public Phase phase;

    public enum Phase
    {
        NONE,
        ACTIVE,
        DECLARE_BLOCKERS,
        WAITING
    }

    // Use this for initialization
    void Start () {
        gameDriver = this;
        cards = new Dictionary<string, Card>();
        startTime = Time.time;
        float optimalWidth = 1600.0f;
        float newScaleFactor = Screen.width / optimalWidth;
        print(Screen.width);
        print(canvasTransform.sizeDelta.x);
        print(newScaleFactor);
        scaleFactor = newScaleFactor;
        canvasScaler.scaleFactor = newScaleFactor;
    }
	
	// Update is called once per frame
	void Update () {
		if (Time.time - startTime > 2)
        {
            List<string> temp = new List<string>();
            temp.Add("Stubborn Veteran");
            temp.Add("Stubborn Veteran");
            temp.Add("Stubborn Veteran");
            temp.Add("Stubborn Veteran");
            temp.Add("Stubborn Veteran");
            temp.Add("Stubborn Veteran");
            temp.Add("Stubborn Veteran");
            temp.Add("Stubborn Veteran");
            temp.Add("Stubborn Veteran");
            temp.Add("Stubborn Veteran");
            temp.Add("Stubborn Veteran");
            temp.Add("Stubborn Veteran");
            temp.Add("Stubborn Veteran");
            temp.Add("Bionics Factory");

            startTime = 100000;
            ServerActions.startGame(temp, (JSONObject responseObj)=>{
                setupGame(responseObj);
            });
        }
	}

    public PlacementZone getZoneFromString(string playerStr, string zoneStr)
    {
        PlacementZone placementZone = null;
        GameObject obj = null;
        switch (playerStr.ToLower())
        {
            case "one":
                switch(zoneStr.ToLower())
                {
                    case "hand":
                        obj = playerOneHand;
                        break;
                    case "mech":
                        obj = playerOneMech;
                        break;
                    case "clinic":
                        obj = playerOneClinic;
                        break;
                    case "left":
                        obj = playerOneLeft;
                        break;
                    case "center":
                        obj = playerOneCenter;
                        break;
                    case "right":
                        obj = playerOneRight;
                        break;
                }
                break;
            case "two":
                switch (zoneStr.ToLower())
                {
                    case "hand":
                        obj = playerTwoHand;
                        break;
                    case "mech":
                        obj = playerTwoMech;
                        break;
                    case "clinic":
                        obj = playerTwoClinic;
                        break;
                    case "left":
                        obj = playerTwoLeft;
                        break;
                    case "center":
                        obj = playerTwoCenter;
                        break;
                    case "right":
                        obj = playerTwoRight;
                        break;
                }
                break;
        }
        if (obj != null)
        {
            placementZone = obj.GetComponent<PlacementZone>();
        }
        return placementZone;
    }

    public void updatePlayers(JSONObject json)
    {
        List<JSONObject> playerObjs = json.GetField("data").GetField("players").list;
        foreach (JSONObject playerObj in playerObjs)
        {
            string playerName = Player.getPlayerNameFromJson(playerObj);
            if (playerName == "one")
            {
                playerOne.updateFromJson(playerObj);
            }
            else if (playerName == "two")
            {
                playerTwo.updateFromJson(playerObj);
            }
        }
    }

    public void updateCards(JSONObject json)
    {
        List<JSONObject> cardObjs = json.GetField("data").GetField("cards").list;
        foreach (JSONObject cardObj in cardObjs)
        {
            Card card;
            string idStr = cardObj.GetField("gameID").str;
            print(idStr);
            print(cardObj.GetField("name").str);
            if (!cards.ContainsKey(idStr))
            {
                card = InstantiateCard(Card.getTypeFromJson(cardObj));
                if (card != null)
                {
                    card.updateFromJson(cardObj);
                    cards.Add(card.gameID, card);
                }
            }
            else
            {
                card = cards[idStr];
                card.updateFromJson(cardObj);
            }
        }
    }

    public void setupGame(JSONObject json)
    {
        updatePlayers(json);
        List<JSONObject> cardObjs = json.GetField("data").GetField("cards").list;
        foreach (JSONObject cardObj in cardObjs)
        {
            Card card = InstantiateCard(Card.getTypeFromJson(cardObj));
            if (card != null)
            {
                card.updateFromJson(cardObj);
                cards.Add(card.gameID, card);
            }
        }
        foreach(Card card in cards.Values)
        {
            temp2.Add(card);
        }
        // TODO: change based on player types
        activePlayer = playerOne;
        setPhase(Phase.ACTIVE);
    }

    public void setPhase(Phase newPhase)
    {
        phase = newPhase;
        if (phase == Phase.ACTIVE)
        {
            foreach (Card card in cards.Values)
            {
                //print(card.zone);
                //print(card.cardName);
                //print(activePlayer.playerName);
                if (card.zone == "hand" && card.owner == activePlayer.playerName)
                {
                    if (card.cost <= activePlayer.bank)
                    {
                        card.showPlayable();
                    }
                }
                if ((card.zone == "left" || card.zone == "center" || card.zone == "right")
                    && card.owner == activePlayer.playerName)
                {
                    UnitCard unit = card as UnitCard;
                    if (unit != null && unit.cooldown == 0)
                    {
                        card.showPlayable();
                    }
                }
            }
        }
    }

    public Card InstantiateCard(Card.Type type)
    {
        GameObject obj;
        switch (type)
        {
            case Card.Type.fighter:
                obj = Instantiate(fighterPrefab);
                obj.transform.localScale *= GameDriver.gameDriver.scaleFactor;
                return obj.GetComponent<UnitCard>() as Card;
            case Card.Type.specialist:
                obj = Instantiate(specialistPrefab);
                obj.transform.localScale *= GameDriver.gameDriver.scaleFactor;
                return obj.GetComponent<UnitCard>() as Card;
        }
        return null;
    }

    public void playCard(Card card)
    {
        ServerActions.playCard(card.gameID, card.zone, 0, (JSONObject json)=>{
            updatePlayers(json);
            updateCards(json);
        });
    }
}
