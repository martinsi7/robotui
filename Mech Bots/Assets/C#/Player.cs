﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class Player : MonoBehaviour {
    public int health;
    public int debt;
    public bool isDefeated;
    public string playerName;
    public int bank;
    public int attack;
    public Type type = Type.SELF;

    public enum Type
    {
        SELF,
        OTHER,
        COMPUTER
    }

    public static string getPlayerNameFromJson(JSONObject json)
    {
        string retStr = "";
        if (json.HasField("name"))
        {
            retStr = json.GetField("name").str;
        }
        return retStr;
    }

    public void updateFromJson(JSONObject json)
    {
        print(json.ToString());
        if (health != json.GetField("life").i)
        {
            health = (int)json.GetField("life").i;
            var childObj = transform.Find("Health").gameObject;
            childObj.GetComponent<TextMeshProUGUI>().text = health.ToString();
        }
        if (debt != json.GetField("debt").i)
        {
            debt = (int)json.GetField("debt").i;
            var childObj = transform.Find("Debt").gameObject;
            childObj.GetComponent<TextMeshProUGUI>().text = debt.ToString();
            // TODO: enable pay debt if greater than 0 else disable
        }
        if (playerName != json.GetField("name").str)
        {
            playerName = json.GetField("name").str;
        }
        if (bank != json.GetField("bank").i)
        {
            bank = (int)json.GetField("bank").i;
            var childObj = transform.Find("Bank").gameObject;
            childObj.GetComponent<TextMeshProUGUI>().text = bank.ToString();
        }
        if (attack != json.GetField("attack").i)
        {
            attack = (int)json.GetField("attack").i;
            var childObj = transform.Find("Attack").gameObject;
            childObj.GetComponent<TextMeshProUGUI>().text = attack.ToString();
        }
    }

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
