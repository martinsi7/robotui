﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class PlacementZone : Dropies {

    public List<Card> containedCards;
    public bool isLane = false;
    public bool isDebug = false;
    public string zoneName;

    public int maxCards;
    public bool isExtendable;

    public float zoneWidth;

    public PlacementZone()
    {
        containedCards = new List<Card>();
    }

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    // TODO: update to move cards to the right of the released card and rotate;
    public void releaseCard(Card card)
    {
        if (!containedCards.Remove(card))
        {
            print("card " + card.gameID + " was not in list");
            return;
        }
        card.transform.SetParent(null);
    }

    public void addCard(Card card)
    {
        if (card == null)
        {
            print("Draggable was null or not a Card");
            return;
        }
        GameObject go = card.gameObject;
        if (go == null)
        {
            print("Draggable has no game object component");
            return;
        }
        if (card.ownerZone != null)
        {
            card.ownerZone.releaseCard(card);
        }
        card.ownerZone = this;
        card.zone = zoneName;
        containedCards.Add(card);
        card.transform.SetParent(transform);
        card.moveCard(getNextCardPos(card));
    }

    public override bool doDropAction(Draggable d)
    {
        Card card = d as Card;
        if (card == null)
        {
            return false;
        }
        if (card.possibleZones.Contains(this))
        {
            addCard(card);
            return true;
        }
        return false;
    }

    private Vector3 getNextCardPos(Card card)
    {
        float cardBuffer = 5;
        RectTransform cardOutline = card.GetComponent<RectTransform>();
        float cardWidth = (cardOutline.sizeDelta.x + cardBuffer) * GameDriver.gameDriver.scaleFactor;
        float cardHeight = (cardOutline.sizeDelta.y + cardBuffer) * GameDriver.gameDriver.scaleFactor;

        int numCards = 0;
        if (isLane)
        {
            numCards = getNumTypes(card.type);
        }
        else
        {
            numCards = containedCards.Count;
        }
        float adj = (float)numCards - ((float)maxCards / 2) - 0.5f;
        float left = transform.position.x + (adj * cardWidth);
        float height = transform.position.y;
        if (isLane)
        {
            if (card.type == Card.Type.fighter)
            {
                height += cardHeight / 4;
            }
            else
            {
                height -= cardHeight / 4;
                left += cardWidth / 2;
            }
        }
        if (isDebug)
        {
            print(card.cardName);
            print(cardOutline.sizeDelta);
            print(left);
            print(height);
        }
        return new Vector3(left, height);
    }

    public bool canPlace(Card card)
    {
        if (card.isUnit && isLane)
        {
            if (getNumTypes(card.type) < maxCards)
            {
                return true;
            }
        }
        return false;
    }

    private int getNumTypes(Card.Type type)
    {
        int ret = 0;
        foreach (Card card in containedCards)
        {
            if (card.type == type)
            {
                ret++;
            }
        }
        return ret;
    }

    public void showPlayable()
    {
        GetComponent<Image>().material = Resources.Load<Material>("active_card");
    }

    public void showUnplayable()
    {
        GetComponent<Image>().material = Resources.Load<Material>("inactive_zone");
    }

}
