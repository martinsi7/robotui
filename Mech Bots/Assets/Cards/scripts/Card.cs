﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using TMPro;

public class Card : Draggable {
    // card info
    public string           gameID;
    public string           cardName;
    public Faction          faction;
    public Type             type;
    public bool             isUnit;
    public string           zone;
    public int              cost;
    public string owner;

    // unity specific fields
    public bool             isMoving;
    public Vector3          moveTarget;
    public float            movementSpeed = 10;
    public PlacementZone    ownerZone;
    public float            animationTime;

    private bool isPlayable = false;

    public List<PlacementZone> possibleZones;
    private string dragStartZone;
    private Vector3 dragStartPos;

    public static float defaultAnimationTime = 0.5f;

    // Use this for initialization
    void Start()
    {
        type = Type.none;
        possibleZones = new List<PlacementZone>();
    }

    public void moveCard(Vector3 target)
    {
        moveCard(target, defaultAnimationTime);
    }

    public void moveCard(Vector3 target, float timeToMove)
    {
        moveTarget = target;
        animationTime = timeToMove;
        float distance = Vector3.Distance(transform.position, target);
        movementSpeed = distance / animationTime;
        isMoving = true;
        print("inside moveCard for " + cardName);
        print(moveTarget);
    }

    public void showPlayable()
    {
        if (!isPlayable)
        {
            isPlayable = true;
            if (zone == "hand")
            {
                print(cardName);
                isDragEnabled = true;
                updatePossibleZones();
            }
            //print(cardName);
            GetComponent<Image>().material = Resources.Load<Material>("active_card");
        }
    }

    public void updatePossibleZones()
    {
        string[] zones = { "left", "center", "right" };
        foreach (string zoneStr in zones)
        {
            PlacementZone newZone = GameDriver.gameDriver.getZoneFromString(owner, zoneStr);
            //print(newZone.name);
            //print(newZone.canPlace(this));
            if (newZone.canPlace(this))
            {
                possibleZones.Add(newZone);
            }
        }
    }

    public void showUnplayable()
    {
        if (isPlayable)
        {
            isPlayable = false;
            isDragEnabled = false;
            GetComponent<Image>().material = Resources.Load<Material>("inactive_card");
        }
    }

    public enum Type
    {
        none,
        building,
        mech,
        specialist,
        fighter
    };

    public enum Faction
    {
        none,
        neutral,
        old_guard,
        anarchist_youth,
        landlocked_pirates,
        clockwork_monks,
        professionals
    }

    public void updateFromJson(JSONObject json)
    {
        updateInfoFromJson(json);
        updateZoneFromJson(json);
        updateAbilitiesFromJson(json);
    }

    protected virtual void updateInfoFromJson(JSONObject json)
    {
        if (gameID != json.GetField("gameID").str)
        {
            gameID = json.GetField("gameID").str;
        }
        if (cardName != json.GetField("name").str)
        {
            cardName = json.GetField("name").str;
            var childObj = transform.Find("Name").gameObject;
            childObj.GetComponent<TextMeshProUGUI>().text = cardName;
        }
        if (faction != getFactionFromString(json.GetField("faction").str))
        {
            faction = getFactionFromString(json.GetField("faction").str);
            // TODO: implement faction overlay (color?)
        }
        if (cost != json.GetField("cost").i)
        {
            cost = (int)json.GetField("cost").i;
            var childObj = transform.Find("Cost").gameObject;
            childObj.GetComponent<TextMeshProUGUI>().text = cost.ToString();
        }
        if (owner != json.GetField("owner").str)
        {
            owner = json.GetField("owner").str;
        }
    }

    public static Type getTypeFromJson(JSONObject json)
    {
        string cardType = json.GetField("cardType").str;
        if (cardType == "building")
        {
            return Type.building;
        }
        else if (cardType == "mech")
        {
            return Type.mech;
        }
        else if (cardType == "unit")
        {
            string unitType = json.GetField("unitType").str;
            if (unitType == "fighter")
            {
                return Type.fighter;
            }
            else if (unitType == "specialist")
            {
                return Type.specialist;
            }
        }
        return Type.none;
    }

    public static Faction getFactionFromString(string str)
    {
        switch (str.ToLower())
        {
            case "neutral":
                return Faction.neutral;
            case "old guard":
            case "old_guard":
            case "oldguard":
                return Faction.old_guard;
            case "anarchist youth":
            case "anarchist_youth":
            case "anarchistyouth":
                return Faction.anarchist_youth;
            case "landlocked pirates":
            case "landlocked_pirates":
            case "landlockedpirates":
                return Faction.landlocked_pirates;
            case "clockwork monks":
            case "clockwork_monks":
            case "clockworkmonks":
                return Faction.clockwork_monks;
            case "professionals":
                return Faction.professionals;
        }
        return Faction.none;
    }

    protected void updateZoneFromJson(JSONObject json)
    {
        string zoneAll = json.GetField("zone").str;
        string[] parts = zoneAll.Split('-');
        string zoneStr = parts[2];
        string playerStr = parts[1];
        if (zone != zoneStr)
        {
            zone = zoneStr;
            PlacementZone newZone = GameDriver.gameDriver.getZoneFromString(playerStr, zoneStr);
            if (newZone != null)
            {
                newZone.addCard(this);
            }
        }
    }

    protected virtual void updateAbilitiesFromJson(JSONObject json)
    {

    }

    protected void doAnimation()
    {
        if (isMoving)
        {
            Vector3 pos = transform.position;
            pos += Time.deltaTime * Vector3.Normalize(moveTarget - pos) * movementSpeed;
            transform.position = pos;
            if (Vector3.Distance(transform.position, moveTarget) < 30.0f)
            {
                isMoving = false;
                transform.position = moveTarget;
                print("done moving for " + cardName);
                print(moveTarget);
                print(transform.position);
                print(transform.localPosition);
            }
        }
    }
	
	// Update is called once per frame
	void Update () {
        doAnimation();
	
	}

    protected override void handleDragStart()
    {
        base.handleDragStart();
        dragStartZone = zone;
        dragStartPos = transform.position;
        foreach (PlacementZone zone in possibleZones)
        {
            zone.showPlayable();
        }
    }

    protected override void handleDragEnd()
    {
        base.handleDragEnd();
        foreach (PlacementZone zone in possibleZones)
        {
            zone.showUnplayable();
        }
        print(dragStartZone);
        if (zone == dragStartZone)
        {
            // return to original pos
            moveTarget = dragStartPos;
            isMoving = true;
        }
        else if (dragStartZone == "hand")
        {
            GameDriver.gameDriver.playCard(this);
        }
    }
}
