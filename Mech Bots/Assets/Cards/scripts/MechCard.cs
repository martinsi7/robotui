﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class MechCard : Card {

    public int attack;
    public string description;
    public List<string> abilities;

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    protected override void updateInfoFromJson(JSONObject json)
    {
        base.updateInfoFromJson(json);
        if (attack != json.GetField("attack").i)
        {
            attack = (int)json.GetField("attack").i;
            var childObj = transform.Find("Attack").gameObject;
            childObj.GetComponent<TextMeshProUGUI>().text = attack.ToString();
        }
    }
}
