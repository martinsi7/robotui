﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class UnitCard : Card {

    public int attack;
    public int health;
    public string description;
    public List<string> abilities;

    // static abilities
    public bool             mobility;
    public bool             canMove;
    public bool             extortion;
    public bool             aggressive;
    public bool             defensive;
    public bool             reach;
    public bool             tenacity;
    public bool             shambling;
    public bool             mechanized;
    public int cooldown;

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        doAnimation();
	}

    protected override void updateInfoFromJson(JSONObject json)
    {
        base.updateInfoFromJson(json);
        if (attack != json.GetField("attack").i)
        {
            attack = (int)json.GetField("attack").i;
            var childObj = transform.Find("Attack").gameObject;
            childObj.GetComponent<TextMeshProUGUI>().text = attack.ToString();
        }
        if (health != json.GetField("health").i)
        {
            health = (int)json.GetField("health").i;
            var childObj = transform.Find("Health").gameObject;
            childObj.GetComponent<TextMeshProUGUI>().text = attack.ToString();
        }
    }

    protected override void updateAbilitiesFromJson(JSONObject json)
    {
        base.updateAbilitiesFromJson(json);
        //List<string> addedAbilities = new List<string>();
        //List<string> removedAbilities = new List<string>();
        if (json.HasField("cooldown") && json.GetField("cooldown").i != cooldown)
        {
            cooldown = (int)json.GetField("cooldown").i;
            // icon for this is only circle arrow
            // shader is done by GameDriver on turn/phase change
            if (cooldown > 0)
                addIcon("cooldown");
            else
                removeIcon("cooldown");
        }
        if (json.HasField("mobility") && json.GetField("mobility").b != mobility)
        {
            mobility = json.GetField("mobility").b;
            if (mobility)
                addIcon("mobility");
            else
                removeIcon("mobility");
        }
        if (json.HasField("canMove") && json.GetField("canMove").b != canMove)
        {
            canMove = json.GetField("canMove").b;
        }
        if (json.HasField("shambling") && json.GetField("shambling").b != shambling)
        {
            shambling = json.GetField("shambling").b;
            if (shambling)
                addIcon("shambling");
            else
                removeIcon("shambling");
        }
        if (json.HasField("extortion") && json.GetField("extortion").b != extortion)
        {
            extortion = json.GetField("extortion").b;
            if (extortion)
                addIcon("extortion");
            else
                removeIcon("extortion");
        }
        if (json.HasField("aggressive") && json.GetField("aggressive").b != aggressive)
        {
            aggressive = json.GetField("aggressive").b;
            if (aggressive)
                addIcon("aggressive");
            else
                removeIcon("aggressive");
        }
        if (json.HasField("defensive") && json.GetField("defensive").b != defensive)
        {
            defensive = json.GetField("defensive").b;
            if (defensive)
                addIcon("defensive");
            else
                removeIcon("defensive");
        }
        if (json.HasField("reach") && json.GetField("reach").b != reach)
        {
            reach = json.GetField("reach").b;
            if (reach)
                addIcon("reach");
            else
                removeIcon("reach");
        }
        if (json.HasField("tenacity") && json.GetField("tenacity").b != tenacity)
        {
            tenacity = json.GetField("tenacity").b;
            if (tenacity)
                addIcon("tenacity");
            else
                removeIcon("tenacity");
        }
        if (json.HasField("mechanized") && json.GetField("mechanized").b != mechanized)
        {
            mechanized = json.GetField("mechanized").b;
            if (mechanized)
                addIcon("mechanized");
            else
                removeIcon("mechanized");
        }

    }

    public void addIcon(string name)
    {


    }

    public void removeIcon(string name)
    {

    }
}
