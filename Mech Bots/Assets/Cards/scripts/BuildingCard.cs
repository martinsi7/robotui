﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class BuildingCard : Card {

    public bool ready;
    public int progress;

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    protected override void updateAbilitiesFromJson(JSONObject json)
    {
        base.updateAbilitiesFromJson(json);

        if (progress != json.GetField("progress").i)
        {
            progress = (int)json.GetField("progress").i;
            var childObj = transform.Find("Power").gameObject;
            childObj.GetComponent<TextMeshProUGUI>().text = progress.ToString() + "/10";
        }
        if (ready != json.GetField("ready").b)
        {
            ready = json.GetField("ready").b;
            // TODO: change gameobject when ready, overlay?
        }
    }


}
